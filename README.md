# Layerd IDM app

LayeredIDM app

To run you need  
Java 11 (minimum 8)  
Maven  
Git  

## Package instruction
- java-springboot-rest-tla: business layer app
- presentLayer: present layer app

## Present layer installation
````
cd presentLayer; mvn package
````

## Business layer installation
````
cd java-springboot-rest-tla; mvn install
````
## Start the service
````
cd java-springboot-rest-tla; mvn spring-boot:run
````

## Uninstallation
````
cd java-springboot-rest-tla; mvn clean
cd presentLayer; mvn clean
````
