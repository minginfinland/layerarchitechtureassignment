package layerAssignment;
import java.util.Scanner;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 * Hello world!
 *
 */
public class App 
{
    private static final String USER_AGENT = "Mozilla/5.0";
    public static void main( String[] args ) throws IOException
    {
        Scanner myInput = new Scanner(System.in);
        int choice = 0;
        String institution = "";
        String fname = "";
        String lname = "";
        System.out.println("Enter you first name, last namea institution.");
        System.out.println("Hint: Input 1 for TAMK, 2 for TAU, 3 for Normaalikoulu.");
        System.out.println("Input sample:");
        System.out.println("Tim");
        System.out.println("Cook");
        System.out.println("1");
        System.out.println("The input above means a person whose name is Tim Cook studying in TAMK");
        fname = myInput.nextLine();
        lname = myInput.nextLine();
        choice = myInput.nextInt();
        switch(choice){
            case 1: institution = "TAMK";
            break;
            case 2: institution = "TAU";
            break;
            case 3: institution = "Normaalikoulu";
            break;
            default: break;
        }
        String finalString = "http://localhost:8471/api/" + institution + "/" + fname + "/" + lname;
        System.out.println(finalString);
        URL requestUrl = new URL(finalString);
        HttpURLConnection con = (HttpURLConnection) requestUrl.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		System.out.println("GET Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
            System.out.println("User is successfully regestered.");
			System.out.println(response.toString());
		} else {
			System.out.println("GET request not worked");
		}
    }
}
